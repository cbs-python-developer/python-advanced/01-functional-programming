"""Homework 1 Task 6 implementation."""

import functools
from typing import Iterator, Callable


def even(func: Callable[[int], Iterator[int]]) -> Callable[[int], Iterator[int]]:
    """Filter out odd numbers."""
    @functools.wraps(func)
    def wrapper(*args, **kwargs) -> Iterator[int]:
        for number in func(*args, **kwargs):
            if number % 2 == 0:
                yield number
    return wrapper


@functools.lru_cache(maxsize=16)
def fibonacci_of(n: int) -> int:
    """Return fibonacci of N."""
    if n < 2:
        return n
    return fibonacci_of(n - 1) + fibonacci_of(n - 2)


@even
def get_fibonacci_sequence(n: int) -> Iterator:
    """Return Fibonacci sequence with the first N numbers."""
    for i in range(n):
        fibonacci = fibonacci_of(i)
        yield fibonacci


if __name__ == '__main__':

    gen1 = get_fibonacci_sequence(17)
    print(gen1, type(gen1))

    for num in gen1:
        print(num, end=' ')
