"""Homework 1 Task 2 implementation."""

import functools
import time
from typing import Any, Callable


def time_meter(func: Callable) -> Callable:
    """Measure the execution time of a function."""
    @functools.wraps(func)
    def _wrapper(*args, **kwargs) -> Any:
        """Inner function."""
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        print(
            f"Function '{func.__name__}' took {end_time - start_time:.7f} seconds."
        )
        return result
    return _wrapper


@time_meter
def sleep(seconds: int) -> None:
    """Sleep for specified number of seconds."""
    time.sleep(seconds)


if __name__ == '__main__':

    sleep(3)
