"""Homework 1 Task 3, 4 implementation."""

import functools
from typing import Callable

from hw12_time_meter import time_meter


def fibonacci_of(n: int) -> int:
    """Return fibonacci of N."""
    if n < 2:
        return n
    return fibonacci_of(n - 1) + fibonacci_of(n - 2)


@functools.cache
def fibonacci_cached(n: int) -> int:
    """Return fibonacci of N."""
    if n < 2:
        return n
    return fibonacci_cached(n - 1) + fibonacci_cached(n - 2)


@functools.lru_cache(maxsize=10)
def fibonacci_lru_10(n: int) -> int:
    """Return fibonacci of N."""
    if n < 2:
        return n
    return fibonacci_lru_10(n - 1) + fibonacci_lru_10(n - 2)


@functools.lru_cache(maxsize=16)
def fibonacci_lru_16(n: int) -> int:
    """Return fibonacci of N."""
    if n < 2:
        return n
    return fibonacci_lru_16(n - 1) + fibonacci_lru_16(n - 2)


@time_meter
def get_fibonacci_sequence(n: int, fibonacci_func: Callable[[int], int]) -> None:
    """Print Fibonacci sequence with the first N numbers."""
    for i in range(n):
        fibonacci = fibonacci_func(i)
        print(fibonacci, end=' ')
    print()


if __name__ == '__main__':

    fibonacci_functions = [
        fibonacci_of,
        fibonacci_cached,
        fibonacci_lru_10,
        fibonacci_lru_16
    ]
    for func in fibonacci_functions:
        print(f"\nFibonacci function: {func.__name__}")
        get_fibonacci_sequence(25, func)
