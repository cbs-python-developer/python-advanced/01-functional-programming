"""Homework 1 Task 7 implementation."""

from typing import Callable


def multiply(a: float, b: float) -> float:
    """Multiply two numbers together."""
    return a * b


def curried_multiply(a: float) -> Callable[[float], float]:
    """Multiply two numbers together."""
    def inner(b: float) -> float:
        return a * b
    return inner


if __name__ == '__main__':

    print(multiply(17, 22))
    print(curried_multiply(17))
    print(curried_multiply(17)(22))
