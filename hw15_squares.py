"""Homework 1 Task 5 implementation."""


integers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
squares_of_odd_numbers = list(
    map(lambda x: x**2,
        filter(
            lambda i: i % 2,
            integers
            )
        )
)
print(squares_of_odd_numbers)
